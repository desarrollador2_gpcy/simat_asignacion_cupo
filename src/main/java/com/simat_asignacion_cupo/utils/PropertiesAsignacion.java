package com.simat_asignacion_cupo.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesAsignacion {


	public String getProperty(String value) {	
		System.out.println("getProperty "+value);
		InputStream input = PropertiesAsignacion.class.getClassLoader().getResourceAsStream("application.properties");
        Properties prop = new Properties();
        String propiedad="";
        try {
			prop.load(input);
			propiedad  = prop.getProperty(value);
		} catch (IOException e) {
			e.printStackTrace();
		}
        
      return propiedad;
	}
}
