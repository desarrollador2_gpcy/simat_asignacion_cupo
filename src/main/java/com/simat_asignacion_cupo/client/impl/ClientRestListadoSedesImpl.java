package com.simat_asignacion_cupo.client.impl;

import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.simat_asignacion_cupo.client.ClientRestListadoSedes;
import com.simat_asignacion_cupo.client.dto.SedeEntity;

@Service
public class ClientRestListadoSedesImpl implements ClientRestListadoSedes{

	@Override
	public List<SedeEntity> getListadoSedes(String codDaneInstitucion, String codDaneSede, String url) {

		System.out.println("validarCupo "+url);
		RestTemplate restTemplate= new RestTemplate();
		StringBuilder builder= new StringBuilder();
		String URI=builder.append(url)
				.append(codDaneInstitucion).append("/").append(codDaneSede).append("/").toString();
		System.out.println("Fin validarCupo "+URI);
		 ResponseEntity<List<SedeEntity>> response = restTemplate.exchange(
				  URI, HttpMethod.GET,null, 
		    new ParameterizedTypeReference<List<SedeEntity>>() {});
		  List<SedeEntity> entities= response.getBody();
		return entities;
	}

}
