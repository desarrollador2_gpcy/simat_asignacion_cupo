package com.simat_asignacion_cupo.client.impl;

import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.simat_asignacion_cupo.client.ClientRestValidarPersona;
import com.simat_asignacion_cupo.client.dto.PersonasEntity;

@Service
public class ClientRestValidarPersonaImpl  implements ClientRestValidarPersona{

	public List<PersonasEntity> validarPersonaExiste(Integer tipoIdentificacion,
			String numeroIdentificacion,
			 String nombreUno, 
			 String nombreDos,
			 String apellidoUno, 
			 String apellidoDos,String fechaNacimiento, String url) throws Exception {
		try {
		
		RestTemplate restTemplate= new RestTemplate();
		StringBuilder builder= new StringBuilder();
		String URI=builder.append(url).append("?")
				.append("tipoIdentificacion=")
				.append(tipoIdentificacion)
				.append("&numeroIdentificacion=")
				.append(numeroIdentificacion).
				append("&nombreUno=")
				.append(nombreUno)
				.append("&nombreDos=")
				.append(nombreDos)
				.append("&apellidoUno=")
				.append(apellidoUno)
				.append("&apellidoDos=")
				.append(apellidoDos)
				.append("&fechaNacimiento=")
				.append(fechaNacimiento).toString();
		
		  ResponseEntity<List<PersonasEntity>> response = restTemplate.exchange(
				  URI, HttpMethod.GET,null, 
		    new ParameterizedTypeReference<List<PersonasEntity>>() {});
		  List<PersonasEntity> entities= response.getBody();
		return entities;
		}
		catch (Exception e) {
			// TODO: handle exception
			throw new Exception("Exception message validarPersonaExiste "+e.getMessage());

		}

	}
}
