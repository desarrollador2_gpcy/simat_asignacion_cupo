package com.simat_asignacion_cupo.client.impl;

import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.simat_asignacion_cupo.client.ClientRestValidarDaneEstablecimiento;
import com.simat_asignacion_cupo.client.dto.InstitucionEntity;
@Service
public class ClientRestValidarDaneEstablecimientoImpl implements ClientRestValidarDaneEstablecimiento{
	
	public List<InstitucionEntity> validarDaneEstablecimiento(String codMunicipio) {
		
		RestTemplate restTemplate= new RestTemplate();
		StringBuilder builder= new StringBuilder();
		String URI=builder.append("http://52.43.129.135:8092/simat/institucion/getInstitucionPublicaByMunicipio/")
				.append(codMunicipio).toString();
	return restTemplate.getForObject(URI, null, new ParameterizedTypeReference<List<InstitucionEntity>>() {});

	}

}
