package com.simat_asignacion_cupo.client.impl;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.simat_asignacion_cupo.client.ClientRestEstadoAlumno;
import com.simat_asignacion_cupo.client.dto.PersonasEstadoEntity;

@Service
public class ClientRestEstadoAlumnoImpl implements ClientRestEstadoAlumno{
	
	@Override
	public PersonasEstadoEntity obtenerEstadoAlumno(String idPersona, String url) {
		System.out.println("Ini obtenerEstadoAlumno "+url);

		RestTemplate restTemplate= new RestTemplate();
		StringBuilder builder= new StringBuilder();
		String URI=builder.append(url)
				.append(idPersona).toString();
		System.out.println("Fin obtenerEstadoAlumno "+URI);

	return restTemplate.getForObject(URI, PersonasEstadoEntity.class);
	
	}

}
