package com.simat_asignacion_cupo.client.impl;

import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.simat_asignacion_cupo.client.ClientRestEtapa;
import com.simat_asignacion_cupo.client.dto.EtapaEntity;
import com.simat_asignacion_cupo.client.dto.PersonasEntity;
import com.simat_asignacion_cupo.client.dto.TablaInstitucionesEntity;

@Service
public class ClientRestEtapaImpl implements ClientRestEtapa {


	@Override
	public List<EtapaEntity> getProcesoByDaneAnoEtapa(String codDane, Integer anio, String codEtapa, String url) {
		System.out.println("Ini getProcesoByDaneAnoEtapa "+url);
		RestTemplate restTemplate= new RestTemplate();
		StringBuilder builder= new StringBuilder();
		String URL=builder.append(url)
				.append(codDane).append("/").append(anio).append("/").append(codEtapa).append("/").toString();
		System.out.println("Fin getProcesoByDaneAnoEtapa "+URL);
	
		  ResponseEntity<List<EtapaEntity>> response = restTemplate.exchange(
				  URL, HttpMethod.GET,null, 
		    new ParameterizedTypeReference<List<EtapaEntity>>() {});
		return response.getBody();		
		
	}

	@Override
	public List<TablaInstitucionesEntity> getInstitucionPublicaByDane(String codDane, String url) {
		System.out.println("Ini getInstitucionPublicaByDane "+url);
		RestTemplate restTemplate= new RestTemplate();
		StringBuilder builder= new StringBuilder();
		String URL=builder.append(url)
				.append(codDane).toString();
		System.out.println("Fin getInstitucionPublicaByDane "+URL);
		  ResponseEntity<List<TablaInstitucionesEntity>> response = restTemplate.exchange(
				  URL, HttpMethod.GET,null, 
		    new ParameterizedTypeReference<List<TablaInstitucionesEntity>>() {});
		return response.getBody();		
	}
}
