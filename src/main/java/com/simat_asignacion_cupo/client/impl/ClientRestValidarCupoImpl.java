package com.simat_asignacion_cupo.client.impl;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.simat_asignacion_cupo.client.ClientRestValidarCupo;
import com.simat_asignacion_cupo.entity.GruposEntityAsig;

@Service
public class ClientRestValidarCupoImpl implements ClientRestValidarCupo {
	
	public GruposEntityAsig validarCupo( Integer ano, String codGrado,  String codMetodologia, String codJornada, 
			 String codDane, String url) {
		System.out.println("validarCupo "+url);
		RestTemplate restTemplate= new RestTemplate();
		StringBuilder builder= new StringBuilder();
		String URI=builder.append(url)
				.append(ano).append("/").append(codGrado).append("/").append(codMetodologia).append("/")
				.append(codJornada).append("/").append(codDane).toString();
		System.out.println("Fin validarCupo "+URI);

		return restTemplate.getForObject(URI, GruposEntityAsig.class);
	}

}
