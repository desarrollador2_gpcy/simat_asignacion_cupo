package com.simat_asignacion_cupo.client.impl;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.simat_asignacion_cupo.client.ClientRestValidarEstadoInscripcion;

@Service
public class ClientRestValidarEstadoInscripcionImpl implements ClientRestValidarEstadoInscripcion{

	public Integer validarEstadoInscripcion(Integer idPersona, String codDane, Integer anio, String url) {
		
		RestTemplate restTemplate= new RestTemplate();
		StringBuilder builder= new StringBuilder();
		Integer inscrito=0;
		String URI=builder.append(url)
				.append(idPersona).append("/estado/inscripcion/").append(anio).append("/").append(codDane).toString();
		Object object=restTemplate.getForObject(URI, Integer.class);
		if(object!= null) {
			 inscrito=(Integer)object;
		}
	return inscrito;
	}
}
