package com.simat_asignacion_cupo.client;

import java.util.List;

import com.simat_asignacion_cupo.client.dto.EtapaEntity;
import com.simat_asignacion_cupo.client.dto.TablaInstitucionesEntity;

public interface ClientRestEtapa {

	
	public List<TablaInstitucionesEntity> getInstitucionPublicaByDane(String codDane, String url);

	public List<EtapaEntity> getProcesoByDaneAnoEtapa(String codDane, Integer Anio, String codEtapa, String url);

}
