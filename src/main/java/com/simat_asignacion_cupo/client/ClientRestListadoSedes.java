package com.simat_asignacion_cupo.client;

import java.util.List;

import com.simat_asignacion_cupo.client.dto.SedeEntity;


public interface ClientRestListadoSedes {
	public List<SedeEntity> getListadoSedes(String codDaneInstitucion, String codDaneSede,  String url);

}
