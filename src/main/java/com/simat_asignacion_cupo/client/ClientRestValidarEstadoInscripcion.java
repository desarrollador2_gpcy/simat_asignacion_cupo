package com.simat_asignacion_cupo.client;

public interface ClientRestValidarEstadoInscripcion {
	public Integer validarEstadoInscripcion(Integer idPersona, String codDane, Integer anio, String url);
}
