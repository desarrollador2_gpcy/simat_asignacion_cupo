package com.simat_asignacion_cupo.client;

import java.util.List;

import com.simat_asignacion_cupo.client.dto.PersonasEntity;

public interface ClientRestValidarPersona {

	public List<PersonasEntity> validarPersonaExiste(Integer tipoIdentificacion,
			String numeroIdentificacion,
			 String nombreUno, 
			 String nombreDos,
			 String apellidoUno, 
			 String apellidoDos,String fechaNacimiento, String url) throws Exception;
}
