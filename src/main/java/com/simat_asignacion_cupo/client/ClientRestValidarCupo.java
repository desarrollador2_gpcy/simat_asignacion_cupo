package com.simat_asignacion_cupo.client;

import com.simat_asignacion_cupo.entity.GruposEntityAsig;

public interface ClientRestValidarCupo {

	public GruposEntityAsig validarCupo( Integer ano, String codGrado,  String codMetodologia, String codJornada, 
			 String codDane, String url);
}
