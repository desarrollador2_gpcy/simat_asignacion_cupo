package com.simat_asignacion_cupo.client.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"id_establecimiento",
"cod_dane_establecimiento",
"nombre_establecimiento",
"direccion_establecimiento",
"grados",
"latitud",
"longitud"
})
public class InstitucionEntity {
    @JsonProperty("id_establecimiento")
    private Integer ID;    
    
    @JsonProperty("cod_dane_establecimiento")
    private String DANE; 
    
    @JsonProperty("nombre_establecimiento")
    private String NOMBRE; 
    
    @JsonProperty("direccion_establecimiento")
    private String DIRECCION; 
    
    @JsonProperty("grados")
    private List<GradosEntity> listGrado; 
    
    @JsonProperty("latitud")
    private double LATITUD; 
    
    @JsonProperty("longitud")
    private double LONGITUD; 
    
    @JsonProperty("id_establecimiento")
    public Integer getIdEstablecimiento() {
        return ID;
    }
    
    @JsonProperty("id_establecimiento")
    public void setIdEstablecimiento(Integer idEstablecimiento) {
        this.ID = idEstablecimiento;
    }
    
    @JsonProperty("cod_dane_establecimiento")
    public String getCodDaneEstablecimiento() {
        return DANE;
    }
    
    @JsonProperty("cod_dane_establecimiento")
    public void setCodDaneEstablecimiento(String codDaneEstablecimiento) {
        this.DANE = codDaneEstablecimiento;
    }
    
    @JsonProperty("nombre_establecimiento")
    public String getNombreEstablecimiento() {
    return NOMBRE;
    }
    
    @JsonProperty("nombre_establecimiento")
    public void setNombreEstablecimiento(String nombreEstablecimiento) {
    this.NOMBRE = nombreEstablecimiento;
    }
    
    @JsonProperty("direccion_establecimiento")
    public String getDireccionEstablecimiento() {
        return DIRECCION;
    }
    
    @JsonProperty("direccion_establecimiento")
    public void setDireccionEstablecimiento(String direccionEstablecimiento) {
        this.DIRECCION = direccionEstablecimiento;
    }
    
    @JsonProperty("grados")
    public List<GradosEntity> getGrado() {
        return listGrado;
    }
    
    @JsonProperty("grados")
    public void setGrado(List<GradosEntity> grado) {
        this.listGrado = grado;
    }
    
    @JsonProperty("latitud")
    public double getLatitud() {
        return LATITUD;
    }
    
    @JsonProperty("latitud")
    public void setLatitud(double latitud) {
        this.LATITUD = latitud;
    }
    
    @JsonProperty("longitud")
    public double getLongitud() {
        return LONGITUD;
    }
    
    @JsonProperty("longitud")
    public void setLongitud(double longitud) {
        this.LONGITUD = longitud;
    }
}