package com.simat_asignacion_cupo.client.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "GRUPOS")
public class GruposEntity {
	
	@Id
	@Column(name = "ID")
	private Integer id;
	
	@Column(name = "COD_GRADO")
	private String codGrado;
	
	@Column(name = "COD_METODOLOGIA")
	private Integer codMetodologia;
	
	@Column(name = "COD_JORNADA")
	private Integer codJornada;
	
	@Column(name = "DANE_INSTITUCION")
	private String daneInstitucion;
	
	@Column(name = "nombre")
	private String nombre;
	
	@Column(name = "DANE_SEDE")
	private String daneSede;
	
	@Column(name = "ANO")
	private Integer ano;
	
	@Column(name = "GRADO")
	private String grado;
	
	@Column(name = "COD_GRADO_2")
	private Integer codGrado2;
	
	@Column(name = "METODOLOGIA")
	private String metodologia;
	
	@Column(name = "jornada")
	private String jornada;

	@Column(name = "CAPACIDAD")
	private Integer capacidad;
	
	@Column(name = "ASIGNADOS")
	private Integer asignados;
	
	@Column(name = "SALDO")
	private Integer saldo;
	
	@Column(name = "RESERVADOS")
	private String reservados;
	
	@Column(name = "CODIGO")
	private String codigo;
	
	@Column(name = "jor_sede")
	private Integer jorSede;

	@Column(name = "cte_METODOLOGIA")
	private Integer cteMETODOLOGIA;

	@Column(name = "consecutivo")
	private Integer consecutivo;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCodGrado() {
		return codGrado;
	}

	public void setCodGrado(String codGrado) {
		this.codGrado = codGrado;
	}

	public Integer getCodMetodologia() {
		return codMetodologia;
	}

	public void setCodMetodologia(Integer codMetodologia) {
		this.codMetodologia = codMetodologia;
	}

	public Integer getCodJornada() {
		return codJornada;
	}

	public void setCodJornada(Integer codJornada) {
		this.codJornada = codJornada;
	}

	public String getDaneInstitucion() {
		return daneInstitucion;
	}

	public void setDaneInstitucion(String daneInstitucion) {
		this.daneInstitucion = daneInstitucion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDaneSede() {
		return daneSede;
	}

	public void setDaneSede(String daneSede) {
		this.daneSede = daneSede;
	}

	public Integer getAno() {
		return ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}

	public String getGrado() {
		return grado;
	}

	public void setGrado(String grado) {
		this.grado = grado;
	}

	public Integer getCodGrado2() {
		return codGrado2;
	}

	public void setCodGrado2(Integer codGrado2) {
		this.codGrado2 = codGrado2;
	}

	public String getMetodologia() {
		return metodologia;
	}

	public void setMetodologia(String metodologia) {
		this.metodologia = metodologia;
	}

	public String getJornada() {
		return jornada;
	}

	public void setJornada(String jornada) {
		this.jornada = jornada;
	}

	public Integer getCapacidad() {
		return capacidad;
	}

	public void setCapacidad(Integer capacidad) {
		this.capacidad = capacidad;
	}

	public Integer getAsignados() {
		return asignados;
	}

	public void setAsignados(Integer asignados) {
		this.asignados = asignados;
	}

	public Integer getSaldo() {
		return saldo;
	}

	public void setSaldo(Integer saldo) {
		this.saldo = saldo;
	}

	public String getReservados() {
		return reservados;
	}

	public void setReservados(String reservados) {
		this.reservados = reservados;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public Integer getJorSede() {
		return jorSede;
	}

	public void setJorSede(Integer jorSede) {
		this.jorSede = jorSede;
	}

	public Integer getCteMETODOLOGIA() {
		return cteMETODOLOGIA;
	}

	public void setCteMETODOLOGIA(Integer cteMETODOLOGIA) {
		this.cteMETODOLOGIA = cteMETODOLOGIA;
	}

	public Integer getConsecutivo() {
		return consecutivo;
	}

	public void setConsecutivo(Integer consecutivo) {
		this.consecutivo = consecutivo;
	}
	
	
	
}
