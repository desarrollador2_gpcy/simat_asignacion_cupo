package com.simat_asignacion_cupo.client.dto;

public class ResultadoDTO {

    private String anio;
    private String error;

    public ResultadoDTO(String anio, String error) {
        this.anio = anio;
        this.error = error;
    }       

    public String getAnio() {
        return anio;
    }
    
    public String getError() {
        return error;
    }

    public void setAnio(String anio) {
        this.anio = anio;
    }
            
    public void setError(String error) {
        this.error = error;
    }
}
