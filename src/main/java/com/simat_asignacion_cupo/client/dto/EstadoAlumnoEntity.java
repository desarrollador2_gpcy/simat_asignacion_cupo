package com.simat_asignacion_cupo.client.dto;

public class EstadoAlumnoEntity {

	private Integer id;

	private CteEntity cteIdEstado;
	

	private CteEntity cteIdGrado;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public CteEntity getCteIdEstado() {
		return cteIdEstado;
	}

	public void setCteIdEstado(CteEntity cteIdEstado) {
		this.cteIdEstado = cteIdEstado;
	}

	public CteEntity getCteIdGrado() {
		return cteIdGrado;
	}

	public void setCteIdGrado(CteEntity cteIdGrado) {
		this.cteIdGrado = cteIdGrado;
	}
	
}
