package com.simat_asignacion_cupo.client.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"id_grado",
"cod_grado",
"nom_grado",
"metodologia"
})
public class GradosEntity {
    @JsonProperty("id_grado")
    private Integer idGrado;    
    
    @JsonProperty("cod_grado")
    private String codGrado; 
    
    @JsonProperty("nom_grado")
    private String nomGrado; 
    
    @JsonProperty("metodologia")
    private List<MetodologiaEntity> listMetodologia; 
    
    @JsonProperty("id_grado")
    public Integer getIdGrado() {
    return idGrado;
    }
    
    @JsonProperty("id_grado")
    public void setIdGrado(Integer idEstablecimiento) {
        this.idGrado = idEstablecimiento;
    }
    
    @JsonProperty("cod_grado")
    public String getCodGrado() {
        return codGrado;
    }
    
    @JsonProperty("cod_grado")
    public void setCodGrado(String codGrado) {
        this.codGrado = codGrado;
    }
    
    @JsonProperty("nom_grado")
    public String getNomGrado() {
        return nomGrado;
    }
    
    @JsonProperty("nom_grado")
    public void setNomGrado(String nomGrado) {
        this.nomGrado = nomGrado;
    }
    
    @JsonProperty("metodologia")
    public List<MetodologiaEntity> getMetodologia() {
        return listMetodologia;
    }
    
    @JsonProperty("metodologia")
    public void setMetodologia(List<MetodologiaEntity> listMetodologia) {
        this.listMetodologia = listMetodologia;
    }
}
