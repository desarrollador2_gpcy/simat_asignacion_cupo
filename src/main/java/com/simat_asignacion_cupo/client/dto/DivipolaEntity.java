package com.simat_asignacion_cupo.client.dto;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "DIVIPOLA")
public class DivipolaEntity {

	@Id
	@Column(name = "ID")
	private Integer id;
	
	@Column(name = "NOMBRE")
	private String nombre;
	
	@Column(name = "CODIGO")
	private String codigo;
	
	@Column(name = "DVP_ID")
	private String dvpId;

	@Column(name = "AUDIT_USER")
	private String auditUser;

	@Column(name = "AUDIT_PAGE")
	private String auditPage;	

	@Column(name = "AUDIT_DATE")
	private Date auditDate;	
	
	@Column(name = "ES_PAIS")
	private String esPais;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDvpId() {
		return dvpId;
	}

	public void setDvpId(String dvpId) {
		this.dvpId = dvpId;
	}

	public String getAuditUser() {
		return auditUser;
	}

	public void setAuditUser(String auditUser) {
		this.auditUser = auditUser;
	}

	public String getAuditPage() {
		return auditPage;
	}

	public void setAuditPage(String auditPage) {
		this.auditPage = auditPage;
	}

	public Date getAuditDate() {
		return auditDate;
	}

	public void setAuditDate(Date auditDate) {
		this.auditDate = auditDate;
	}

	public String getEsPais() {
		return esPais;
	}

	public void setEsPais(String esPais) {
		this.esPais = esPais;
	}
}
	