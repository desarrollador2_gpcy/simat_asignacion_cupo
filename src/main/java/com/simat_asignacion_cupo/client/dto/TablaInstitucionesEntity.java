package com.simat_asignacion_cupo.client.dto;

public class TablaInstitucionesEntity {
    private Integer id;
    private String insType;

    private String dane;
    private String nombre;
    private String rector;
    private String subsidio;
    private String direccion;
    private String web;
    private Integer dvpIdDireccion;
    private Integer jerId;
    private String barrioInspeccion;
    private String licencia;
    private Integer cteIdResguardo;
    private String telefono;
    private Integer cteIdPropiedad;
    private String fax;
    private Integer cteIdCalendario;
    private String email;
    private Integer cteIdTarifa;
    private Integer cteIdPropiedadNoofi;
    private Integer cteIdCostos;
    private String estado;

    public Integer getId() {
		return id;
	}

    public void setId(Integer id) {
            this.id = id;
    }   


    public String getInsType() {
		return insType;
	}

    public void setInsType(String insType) {
            this.insType = insType;
    }

    public String getDane() {
		return dane;
	}

    public void setDane(String dane) {
            this.dane = dane;
    }

    public String getNombre() {
                    return nombre;
            }

    public void setNombre(String nombre) {
            this.nombre = nombre;
    }

    public String getRector() {
                    return rector;
            }

    public void setRector(String rector) {
            this.rector = rector;
    }

    public String getSubsidio() {
                    return subsidio;
            }

    public void setSubsidio(String subsidio) {
            this.subsidio = subsidio;
    }

    public String getDireccion() {
                    return direccion;
            }

    public void setDireccion(String direccion) {
            this.direccion = direccion;
    }

    public String getWeb() {
                    return web;
            }

    public void setWeb(String web) {
            this.web = web;
    }

    public Integer getDvpIdDireccion() {
                    return dvpIdDireccion;
            }

    public void setDvpIdDireccion(Integer dvpIdDireccion) {
            this.dvpIdDireccion = dvpIdDireccion;
    }

    public Integer getJerId() {
                    return jerId;
            }

    public void setJerId(Integer jerId) {
            this.jerId = jerId;
    }

    public String getBarrioInspeccion() {
                    return barrioInspeccion;
            }

    public void setBarrioInspeccion(String barrioInspeccion) {
            this.barrioInspeccion = barrioInspeccion;
    }

    public String getLicencia() {
                    return licencia;
            }

    public void setLicencia(String licencia) {
            this.licencia = licencia;
    }

    public Integer getCteIdResguardo() {
                    return cteIdResguardo;
            }

    public void setCteIdResguardo(Integer cteIdResguardo) {
            this.cteIdResguardo = cteIdResguardo;
    }

    public String getTelefono() {
                    return telefono;
            }

    public void setTelefono(String telefono) {
            this.telefono = telefono;
    }

    public Integer getCteIdPropiedad() {
                    return cteIdPropiedad;
            }

    public void setCteIdPropiedad(Integer cteIdPropiedad) {
            this.cteIdPropiedad = cteIdPropiedad;
    }

    public String getFax() {
                    return fax;
            }

    public void setFax(String fax) {
            this.fax = fax;
    }

    public Integer getCteIdCalendario() {
                    return cteIdCalendario;
            }

    public void setCteIdCalendario(Integer cteIdCalendario) {
            this.cteIdCalendario = cteIdCalendario;
    }

    public String getEmail() {
                    return email;
            }

    public void setEmail(String email) {
            this.email = email;
    }

    public Integer getCteIdTarifa() {
                    return cteIdTarifa;
            }

    public void setCteIdTarifa(Integer cteIdTarifa) {
            this.cteIdTarifa = cteIdTarifa;
    }

    public Integer getCteIdPropiedadNoofi() {
                    return cteIdPropiedadNoofi;
            }

    public void setCteIdPropiedadNoofi(Integer cteIdPropiedadNoofi) {
            this.cteIdPropiedadNoofi = cteIdPropiedadNoofi;
    }

    public Integer getCteIdCostos() {
                    return cteIdCostos;
            }

    public void setCteIdCostos(Integer cteIdCostos) {
            this.cteIdCostos = cteIdCostos;
    }

    public String getEstado() {
		return estado;
	}

    public void setEstado(String estado) {
            this.estado = estado;
    }
}
