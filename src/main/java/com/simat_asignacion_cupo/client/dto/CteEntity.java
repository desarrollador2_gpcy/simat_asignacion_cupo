package com.simat_asignacion_cupo.client.dto;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "CTE")
public class CteEntity {

	@Id
	@Column(name = "ID")
	private Integer id;
	
	@Column(name = "NOMBRE")
	private String nombre;
	
	@Column(name = "CODIGO")
	private String codigo; 
	
	@Column(name = "CTE_ID")
	private Integer cteId;
	
	@Column(name = "AUDIT_USER")
	private String auditUser; 
	
	@Column(name = "AUDIT_PAGE")
	private String auditPage; 
	
	@Column(name = "AUDIT_DATE")
	private Date auditDate; 
	
	@Column(name = "ES_DEL_SISTEMA")
	private String esDelSistema;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public Integer getCteId() {
		return cteId;
	}

	public void setCteId(Integer cteId) {
		this.cteId = cteId;
	}

	public String getAuditUser() {
		return auditUser;
	}

	public void setAuditUser(String auditUser) {
		this.auditUser = auditUser;
	}

	public String getAuditPage() {
		return auditPage;
	}

	public void setAuditPage(String auditPage) {
		this.auditPage = auditPage;
	}

	public Date getAuditDate() {
		return auditDate;
	}

	public void setAuditDate(Date auditDate) {
		this.auditDate = auditDate;
	}

	public String getEsDelSistema() {
		return esDelSistema;
	}

	public void setEsDelSistema(String esDelSistema) {
		this.esDelSistema = esDelSistema;
	}
	
}
	
	