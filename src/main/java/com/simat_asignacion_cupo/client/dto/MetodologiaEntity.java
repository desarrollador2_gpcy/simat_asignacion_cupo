
package com.simat_asignacion_cupo.client.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"id_metodologia",
"cod_metodologia",
"nom_metodologia"
})
public class MetodologiaEntity {
    
    @JsonProperty("id_metodologia")
    private Integer idMetodologia;    
    
    @JsonProperty("cod_metodologia")
    private String codMetodologia; 
    
    @JsonProperty("nom_metodologia")
    private String nomMetodologia; 
        
    @JsonProperty("id_metodologia")
    public Integer getIdMetodologia() {
    return idMetodologia;
    }
    
    @JsonProperty("id_metodologia")
    public void setIdMetodologia(Integer idMetodologia) {
        this.idMetodologia = idMetodologia;
    }
    
    @JsonProperty("cod_metodologia")
    public String getCodMetodologia() {
        return codMetodologia;
    }
    
    @JsonProperty("cod_metodologia")
    public void setCodMetodologia(String codMetodologia) {
        this.codMetodologia = codMetodologia;
    }
    
    @JsonProperty("nom_metodologia")
    public String getNomMetodologia() {
        return nomMetodologia;
    }
    
    @JsonProperty("nom_metodologia")
    public void setNomMetodologia(String nomMetodologia) {
        this.nomMetodologia = nomMetodologia;
    }
    
}
