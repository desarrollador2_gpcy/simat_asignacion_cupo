package com.simat_asignacion_cupo.client.dto;

import java.util.Date;


public class PersonasEstadoEntity {
	

	private Integer id;
	
	private Integer ano; 
	
	private Date fechaInicio; 
	
	private Date fechaFin; 
	
	private String comentario;
	
	private Integer sedeId;

	private Integer cteIdMotivo;
	
	private Integer jSedeId;

	private Integer insId;
	
	private Integer inscId;

	private Integer grpId;

	private Integer cteIdGrado;
	
	private Integer cteIdEstado;

	private Integer estAluIdSiguiente;

	private Integer estAluIdAnterior;
	
	private Integer perId;

	private Integer jerId;

	private String repitente;

	private Integer trasladoId;

	private Integer cteIdMetod;

	private String auditUser;

	private String auditPage;

	private Date auditDate;
	
	private String camCalendario;

	private Integer cteIdEstrato;

	private Integer cteIdNivSisben;

	private Integer cteIdPobVictima;

	private String cerDesplazado;

	private Date fecDesplazado;

	private Integer dvpIdDesplazado;

	private Integer cteIdEspecial;

	private String subsidiado;
 	
	private Integer cteIdZona;

	private Integer cteIdFueRecu;

	private Integer cteIdInternado;
	
 	private Integer conConSedCupoId;

	private Integer porSisben;
	
	private Integer cteTipoDocente;	
	
	private Integer cteIdSrpa;
	
	private Integer cteIdApoyoAcadEsp;
	
	private Integer cteIdPaisOrigen;
	
	private Date fecVenDoc;
	
	private String alternancia;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getAno() {
		return ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public Integer getSedeId() {
		return sedeId;
	}

	public void setSedeId(Integer sedeId) {
		this.sedeId = sedeId;
	}

	public Integer getCteIdMotivo() {
		return cteIdMotivo;
	}

	public void setCteIdMotivo(Integer cteIdMotivo) {
		this.cteIdMotivo = cteIdMotivo;
	}

	public Integer getjSedeId() {
		return jSedeId;
	}

	public void setjSedeId(Integer jSedeId) {
		this.jSedeId = jSedeId;
	}

	public Integer getInsId() {
		return insId;
	}

	public void setInsId(Integer insId) {
		this.insId = insId;
	}

	public Integer getInscId() {
		return inscId;
	}

	public void setInscId(Integer inscId) {
		this.inscId = inscId;
	}

	public Integer getGrpId() {
		return grpId;
	}

	public void setGrpId(Integer grpId) {
		this.grpId = grpId;
	}

	public Integer getCteIdGrado() {
		return cteIdGrado;
	}

	public void setCteIdGrado(Integer cteIdGrado) {
		this.cteIdGrado = cteIdGrado;
	}

	public Integer getCteIdEstado() {
		return cteIdEstado;
	}

	public void setCteIdEstado(Integer cteIdEstado) {
		this.cteIdEstado = cteIdEstado;
	}

	public Integer getEstAluIdSiguiente() {
		return estAluIdSiguiente;
	}

	public void setEstAluIdSiguiente(Integer estAluIdSiguiente) {
		this.estAluIdSiguiente = estAluIdSiguiente;
	}

	public Integer getEstAluIdAnterior() {
		return estAluIdAnterior;
	}

	public void setEstAluIdAnterior(Integer estAluIdAnterior) {
		this.estAluIdAnterior = estAluIdAnterior;
	}

	public Integer getPerId() {
		return perId;
	}

	public void setPerId(Integer perId) {
		this.perId = perId;
	}

	public Integer getJerId() {
		return jerId;
	}

	public void setJerId(Integer jerId) {
		this.jerId = jerId;
	}

	public String getRepitente() {
		return repitente;
	}

	public void setRepitente(String repitente) {
		this.repitente = repitente;
	}

	public Integer getTrasladoId() {
		return trasladoId;
	}

	public void setTrasladoId(Integer trasladoId) {
		this.trasladoId = trasladoId;
	}

	public Integer getCteIdMetod() {
		return cteIdMetod;
	}

	public void setCteIdMetod(Integer cteIdMetod) {
		this.cteIdMetod = cteIdMetod;
	}

	public String getAuditUser() {
		return auditUser;
	}

	public void setAuditUser(String auditUser) {
		this.auditUser = auditUser;
	}

	public String getAuditPage() {
		return auditPage;
	}

	public void setAuditPage(String auditPage) {
		this.auditPage = auditPage;
	}

	public Date getAuditDate() {
		return auditDate;
	}

	public void setAuditDate(Date auditDate) {
		this.auditDate = auditDate;
	}

	public String getCamCalendario() {
		return camCalendario;
	}

	public void setCamCalendario(String camCalendario) {
		this.camCalendario = camCalendario;
	}

	public Integer getCteIdEstrato() {
		return cteIdEstrato;
	}

	public void setCteIdEstrato(Integer cteIdEstrato) {
		this.cteIdEstrato = cteIdEstrato;
	}

	public Integer getCteIdNivSisben() {
		return cteIdNivSisben;
	}

	public void setCteIdNivSisben(Integer cteIdNivSisben) {
		this.cteIdNivSisben = cteIdNivSisben;
	}

	public Integer getCteIdPobVictima() {
		return cteIdPobVictima;
	}

	public void setCteIdPobVictima(Integer cteIdPobVictima) {
		this.cteIdPobVictima = cteIdPobVictima;
	}

	public String getCerDesplazado() {
		return cerDesplazado;
	}

	public void setCerDesplazado(String cerDesplazado) {
		this.cerDesplazado = cerDesplazado;
	}

	public Date getFecDesplazado() {
		return fecDesplazado;
	}

	public void setFecDesplazado(Date fecDesplazado) {
		this.fecDesplazado = fecDesplazado;
	}

	public Integer getDvpIdDesplazado() {
		return dvpIdDesplazado;
	}

	public void setDvpIdDesplazado(Integer dvpIdDesplazado) {
		this.dvpIdDesplazado = dvpIdDesplazado;
	}

	public Integer getCteIdEspecial() {
		return cteIdEspecial;
	}

	public void setCteIdEspecial(Integer cteIdEspecial) {
		this.cteIdEspecial = cteIdEspecial;
	}

	public String getSubsidiado() {
		return subsidiado;
	}

	public void setSubsidiado(String subsidiado) {
		this.subsidiado = subsidiado;
	}

	public Integer getCteIdZona() {
		return cteIdZona;
	}

	public void setCteIdZona(Integer cteIdZona) {
		this.cteIdZona = cteIdZona;
	}

	public Integer getCteIdFueRecu() {
		return cteIdFueRecu;
	}

	public void setCteIdFueRecu(Integer cteIdFueRecu) {
		this.cteIdFueRecu = cteIdFueRecu;
	}

	public Integer getCteIdInternado() {
		return cteIdInternado;
	}

	public void setCteIdInternado(Integer cteIdInternado) {
		this.cteIdInternado = cteIdInternado;
	}

	public Integer getConConSedCupoId() {
		return conConSedCupoId;
	}

	public void setConConSedCupoId(Integer conConSedCupoId) {
		this.conConSedCupoId = conConSedCupoId;
	}

	public Integer getPorSisben() {
		return porSisben;
	}

	public void setPorSisben(Integer porSisben) {
		this.porSisben = porSisben;
	}

	public Integer getCteTipoDocente() {
		return cteTipoDocente;
	}

	public void setCteTipoDocente(Integer cteTipoDocente) {
		this.cteTipoDocente = cteTipoDocente;
	}

	public Integer getCteIdSrpa() {
		return cteIdSrpa;
	}

	public void setCteIdSrpa(Integer cteIdSrpa) {
		this.cteIdSrpa = cteIdSrpa;
	}

	public Integer getCteIdApoyoAcadEsp() {
		return cteIdApoyoAcadEsp;
	}

	public void setCteIdApoyoAcadEsp(Integer cteIdApoyoAcadEsp) {
		this.cteIdApoyoAcadEsp = cteIdApoyoAcadEsp;
	}

	public Integer getCteIdPaisOrigen() {
		return cteIdPaisOrigen;
	}

	public void setCteIdPaisOrigen(Integer cteIdPaisOrigen) {
		this.cteIdPaisOrigen = cteIdPaisOrigen;
	}

	public Date getFecVenDoc() {
		return fecVenDoc;
	}

	public void setFecVenDoc(Date fecVenDoc) {
		this.fecVenDoc = fecVenDoc;
	}

	public String getAlternancia() {
		return alternancia;
	}

	public void setAlternancia(String alternancia) {
		this.alternancia = alternancia;
	}
	
	
	

}
