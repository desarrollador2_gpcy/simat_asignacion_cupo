package com.simat_asignacion_cupo.client.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;



@Entity(name = "SEDES")
public class SedeEntity {

	@Id
	@Column(name = "ID", nullable = true)
	private Long id;

	@Column(name = "INS_ID", nullable = true)
	private Integer insId;

	@Column(name = "DANE", nullable = true)
	private String dane;

	@Column(name = "DANE_ANTERIOR", nullable = true)
	private String daneAnterior;

	@Column(name = "CONSECUTIVO", nullable = true)
	private String consecutivo;

	@Column(name = "PRINCIPAL", nullable = true)
	private String principal;

	@Column(name = "NOMBRE", nullable = true)
	private String nombre;

	@Column(name = "DIRECCION", nullable = true)
	private String direccion;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "DVP_ID_DIRECCION", nullable = true)
	private DivipolaEntity dvpIdDireccion;

	@Column(name = "BARRIO_INSPECCION", nullable = true)
	private String barrioInspeccion;

	@Column(name = "TELEFONO", nullable = true)
	private String telefono;

	@Column(name = "FAX", nullable = true)
	private String fax;

	@Column(name = "EMAIL", nullable = true)
	private String email;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "CTE_ID_ZONA", nullable = true)
	private CteEntity cteIdZona;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "CTE_ID_ETNIA", nullable = true)
	private CteEntity cteIdEtnia;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "CTE_ID_ESPECIALIDAD", nullable = true)
	private CteEntity cteIdEspecialidad;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "CTE_ID_DISCAPACIDAD", nullable = true)
	private CteEntity cteIdDiscapacidad;

	@Column(name = "ESTADO_SINCRONIZADO", nullable = true)
	private Integer estadoSincronizado;

	@Column(name = "LATITUD", nullable = true)
	private Float latitud;

	@Column(name = "LONGITUD", nullable = true)
	private Float longitud;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getInsId() {
		return insId;
	}

	public void setInsId(Integer insId) {
		this.insId = insId;
	}

	public String getDane() {
		return dane;
	}

	public void setDane(String dane) {
		this.dane = dane;
	}

	public String getDaneAnterior() {
		return daneAnterior;
	}

	public void setDaneAnterior(String daneAnterior) {
		this.daneAnterior = daneAnterior;
	}

	public String getConsecutivo() {
		return consecutivo;
	}

	public void setConsecutivo(String consecutivo) {
		this.consecutivo = consecutivo;
	}

	public String getPrincipal() {
		return principal;
	}

	public void setPrincipal(String principal) {
		this.principal = principal;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public DivipolaEntity getDvpIdDireccion() {
		return dvpIdDireccion;
	}

	public void setDvpIdDireccion(DivipolaEntity dvpIdDireccion) {
		this.dvpIdDireccion = dvpIdDireccion;
	}

	public String getBarrioInspeccion() {
		return barrioInspeccion;
	}

	public void setBarrioInspeccion(String barrioInspeccion) {
		this.barrioInspeccion = barrioInspeccion;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public CteEntity getCteIdZona() {
		return cteIdZona;
	}

	public void setCteIdZona(CteEntity cteIdZona) {
		this.cteIdZona = cteIdZona;
	}

	public CteEntity getCteIdEtnia() {
		return cteIdEtnia;
	}

	public void setCteIdEtnia(CteEntity cteIdEtnia) {
		this.cteIdEtnia = cteIdEtnia;
	}

	public CteEntity getCteIdEspecialidad() {
		return cteIdEspecialidad;
	}

	public void setCteIdEspecialidad(CteEntity cteIdEspecialidad) {
		this.cteIdEspecialidad = cteIdEspecialidad;
	}

	public CteEntity getCteIdDiscapacidad() {
		return cteIdDiscapacidad;
	}

	public void setCteIdDiscapacidad(CteEntity cteIdDiscapacidad) {
		this.cteIdDiscapacidad = cteIdDiscapacidad;
	}

	public Integer getEstadoSincronizado() {
		return estadoSincronizado;
	}

	public void setEstadoSincronizado(Integer estadoSincronizado) {
		this.estadoSincronizado = estadoSincronizado;
	}

	public Float getLatitud() {
		return latitud;
	}

	public void setLatitud(Float latitud) {
		this.latitud = latitud;
	}

	public Float getLongitud() {
		return longitud;
	}

	public void setLongitud(Float longitud) {
		this.longitud = longitud;
	}

}

