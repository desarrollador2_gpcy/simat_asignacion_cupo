package com.simat_asignacion_cupo.client.dto;

import java.util.Date;

public class PersonasEstadoInscripcionEntity {

	private Integer id;
	
	private Integer ano;

	private Integer numFormulario;	
	
	private Date fecDigitacion;	
	
	private Date fecRecepcion;	

	private String comentario;
	
	private Integer jerId;
	
	private Integer cteIdEstadoIns;
	
	private Integer perIdAlumno;

	private Integer cteIdAlumno;

	private String cancelada;
	
	private String vieneSectorPrivado;
	
	private String vieneOtroMunicipio;
	
	private String institucionBienestar;
	
	private Integer cteIdSituacionAnoAnterior;

	private String auditUser;

	private String auditPage;
	
	private Date auditDate;
	
	private String camCalenadrio;
	
	private String nombreAcudiente;

	private String email;
		
	private String telAcudiente;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getAno() {
		return ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}

	public Integer getNumFormulario() {
		return numFormulario;
	}

	public void setNumFormulario(Integer numFormulario) {
		this.numFormulario = numFormulario;
	}

	public Date getFecDigitacion() {
		return fecDigitacion;
	}

	public void setFecDigitacion(Date fecDigitacion) {
		this.fecDigitacion = fecDigitacion;
	}

	public Date getFecRecepcion() {
		return fecRecepcion;
	}

	public void setFecRecepcion(Date fecRecepcion) {
		this.fecRecepcion = fecRecepcion;
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public Integer getJerId() {
		return jerId;
	}

	public void setJerId(Integer jerId) {
		this.jerId = jerId;
	}

	public Integer getCteIdEstadoIns() {
		return cteIdEstadoIns;
	}

	public void setCteIdEstadoIns(Integer cteIdEstadoIns) {
		this.cteIdEstadoIns = cteIdEstadoIns;
	}

	public Integer getPerIdAlumno() {
		return perIdAlumno;
	}

	public void setPerIdAlumno(Integer perIdAlumno) {
		this.perIdAlumno = perIdAlumno;
	}

	public Integer getCteIdAlumno() {
		return cteIdAlumno;
	}

	public void setCteIdAlumno(Integer cteIdAlumno) {
		this.cteIdAlumno = cteIdAlumno;
	}

	public String getCancelada() {
		return cancelada;
	}

	public void setCancelada(String cancelada) {
		this.cancelada = cancelada;
	}

	public String getVieneSectorPrivado() {
		return vieneSectorPrivado;
	}

	public void setVieneSectorPrivado(String vieneSectorPrivado) {
		this.vieneSectorPrivado = vieneSectorPrivado;
	}

	public String getVieneOtroMunicipio() {
		return vieneOtroMunicipio;
	}

	public void setVieneOtroMunicipio(String vieneOtroMunicipio) {
		this.vieneOtroMunicipio = vieneOtroMunicipio;
	}

	public String getInstitucionBienestar() {
		return institucionBienestar;
	}

	public void setInstitucionBienestar(String institucionBienestar) {
		this.institucionBienestar = institucionBienestar;
	}

	public Integer getCteIdSituacionAnoAnterior() {
		return cteIdSituacionAnoAnterior;
	}

	public void setCteIdSituacionAnoAnterior(Integer cteIdSituacionAnoAnterior) {
		this.cteIdSituacionAnoAnterior = cteIdSituacionAnoAnterior;
	}

	public String getAuditUser() {
		return auditUser;
	}

	public void setAuditUser(String auditUser) {
		this.auditUser = auditUser;
	}

	public String getAuditPage() {
		return auditPage;
	}

	public void setAuditPage(String auditPage) {
		this.auditPage = auditPage;
	}

	public Date getAuditDate() {
		return auditDate;
	}

	public void setAuditDate(Date auditDate) {
		this.auditDate = auditDate;
	}

	public String getCamCalenadrio() {
		return camCalenadrio;
	}

	public void setCamCalenadrio(String camCalenadrio) {
		this.camCalenadrio = camCalenadrio;
	}

	public String getNombreAcudiente() {
		return nombreAcudiente;
	}

	public void setNombreAcudiente(String nombreAcudiente) {
		this.nombreAcudiente = nombreAcudiente;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelAcudiente() {
		return telAcudiente;
	}

	public void setTelAcudiente(String telAcudiente) {
		this.telAcudiente = telAcudiente;
	}
	
	
}
