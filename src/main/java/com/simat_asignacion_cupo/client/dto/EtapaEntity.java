package com.simat_asignacion_cupo.client.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"ANO",
"ABIERTA",
"JER_ID",
"ID_PROCESO"
})
public class EtapaEntity {
    @JsonProperty("ANO")
    private Integer ano;    
    
    @JsonProperty("ABIERTA")
    private String abierta; 

    @JsonProperty("JER_ID")
    private Integer jerId;  
    
    @JsonProperty("ID_PROCESO")
    private Integer idProceso; 
    
    @JsonProperty("ANO")
    public Integer getAno() {
    return ano;
    }
    
    @JsonProperty("ANO")
    public void setAno(Integer ano) {
        this.ano = ano;
    }
    
    @JsonProperty("ABIERTA")
    public String getAbierta() {
        return abierta;
    }
    
    @JsonProperty("ABIERTA")
    public void setAbierta(String abierta) {
        this.abierta = abierta;
    }
    
    @JsonProperty("JER_ID")
    public Integer getJerId() {
    return jerId;
    }
    
    @JsonProperty("JER_ID")
    public void setJerId(Integer jerId) {
        this.jerId = jerId;
    }
    
    @JsonProperty("ID_PROCESO")
    public Integer getIdProceso() {
    return idProceso;
    }
    
    @JsonProperty("ID_PROCESO")
    public void setIdProceso(Integer idProceso) {
        this.idProceso = idProceso;
    }
}