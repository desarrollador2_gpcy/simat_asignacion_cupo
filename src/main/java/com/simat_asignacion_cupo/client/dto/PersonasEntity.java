package com.simat_asignacion_cupo.client.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties(ignoreUnknown = true)
public class PersonasEntity {
	
	private Integer id;
	
	private String perType;
	
	private Integer cteIdTipoDoc;
	
	private String doc; 
	
	private Integer dvpIdExpedicion;
	
	private String apellido1;
	
	private String apellido2;
	
	private String nombre1;
	
	private String nombre2;
	
	private String direccion;
	
	private Integer dvpIdDireccion;
	
	private String barrioInspeccion;
	
	private String telefono;
	
	private String email;
	
	private Date fechaNacimiento;
	
	private Integer dvpIdNacioEn;
	
	private CteEntity cteIdGenero;
	
	private CteEntity cteIdEtnia;
	
	private CteEntity cteIdEstrato;
	
	private CteEntity cteIdEps;
	
	private CteEntity cteIdArs;
	
	private CteEntity cteIdResguardo;
	
	private String ips;

	private CteEntity cteIdNivSisben;
	
	private Integer numSisben;
	
	private CteEntity cteIdPobVictima;
	
	private String cerDesplazado;
	
	private Date fecDesplazado;
	
	private Integer dvpIdDesplazado;
	
	private String idUnico;
	
	private EstadoAlumnoEntity estadoId;
	
	private String nombre1Fnt;
	
	private String nombre2Fnt;
	
	private String apellido1Fnt;
	
	private String apellido2Fnt;
	
	private CteEntity cteIdEspecial;
	
	private String vieneSectorPrivado;
		
	private String vieneOtroMunicipio;
	
	private String institucionBienestar;
	
	private String subsidio;
	
	private CteEntity cteIdSituacionAnoAnterior;
	
	private CteEntity cteIdSangre;
	
	private CteEntity cteIdZona;
	
	private String nuevoEnInstEd;
	
	private String auditUser;
	
	private String auditPage;
	
	private Date auditDate;
	
	private CteEntity cteIdFueRecu;
	
	private String cabFamilia;
	
	private String benMadFlia;
	
	private String benVetFp;
	
	private String benHerNac;
	
	private Integer sinebId;
	
	private Integer sistema;
	
	private Integer tipCruResgistraduria;
	
	private CteEntity cteIdVieneSipi;
	
	private String idUnicoRpt;
	
	private String estSipod;
	
	private String estIndigena;
	
	private String sisbenCruce;
	
	private String sifaId;
	
	private String procesado;
	
	private String porSisben;

	private CteEntity cteIdPaisOrigen;
	
	private Date fecVenDoc;
	
	private Integer gemelo;
		
	private Integer idBenSsdipi;

	private CteEntity cteIdSisbenIv;
	
	private CteEntity cteIdDocVenezolano;
	
	private Integer numVenezolano;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPerType() {
		return perType;
	}

	public void setPerType(String perType) {
		this.perType = perType;
	}

	public Integer getCteIdTipoDoc() {
		return cteIdTipoDoc;
	}

	public void setCteIdTipoDoc(Integer cteIdTipoDoc) {
		this.cteIdTipoDoc = cteIdTipoDoc;
	}

	public String getDoc() {
		return doc;
	}

	public void setDoc(String doc) {
		this.doc = doc;
	}

	public Integer getDvpIdExpedicion() {
		return dvpIdExpedicion;
	}

	public void setDvpIdExpedicion(Integer dvpIdExpedicion) {
		this.dvpIdExpedicion = dvpIdExpedicion;
	}

	public String getApellido1() {
		return apellido1;
	}

	public void setApellido1(String apellido1) {
		this.apellido1 = apellido1;
	}

	public String getApellido2() {
		return apellido2;
	}

	public void setApellido2(String apellido2) {
		this.apellido2 = apellido2;
	}

	public String getNombre1() {
		return nombre1;
	}

	public void setNombre1(String nombre1) {
		this.nombre1 = nombre1;
	}

	public String getNombre2() {
		return nombre2;
	}

	public void setNombre2(String nombre2) {
		this.nombre2 = nombre2;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public Integer getDvpIdDireccion() {
		return dvpIdDireccion;
	}

	public void setDvpIdDireccion(Integer dvpIdDireccion) {
		this.dvpIdDireccion = dvpIdDireccion;
	}

	public String getBarrioInspeccion() {
		return barrioInspeccion;
	}

	public void setBarrioInspeccion(String barrioInspeccion) {
		this.barrioInspeccion = barrioInspeccion;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	
	public Integer getDvpIdNacioEn() {
		return dvpIdNacioEn;
	}
	
	public void setDvpIdNacioEn(Integer dvpIdNacioEn) {
		this.dvpIdNacioEn = dvpIdNacioEn;
	}
	
	public CteEntity getCteIdGenero() {
		return cteIdGenero;
	}

	public void setCteIdGenero(CteEntity cteIdGenero) {
		this.cteIdGenero = cteIdGenero;
	}

	public CteEntity getCteIdEtnia() {
		return cteIdEtnia;
	}

	public void setCteIdEtnia(CteEntity cteIdEtnia) {
		this.cteIdEtnia = cteIdEtnia;
	}

	public CteEntity getCteIdEstrato() {
		return cteIdEstrato;
	}

	public void setCteIdEstrato(CteEntity cteIdEstrato) {
		this.cteIdEstrato = cteIdEstrato;
	}

	public CteEntity getCteIdEps() {
		return cteIdEps;
	}

	public void setCteIdEps(CteEntity cteIdEps) {
		this.cteIdEps = cteIdEps;
	}

	public CteEntity getCteIdArs() {
		return cteIdArs;
	}

	public void setCteIdArs(CteEntity cteIdArs) {
		this.cteIdArs = cteIdArs;
	}

	public CteEntity getCteIdResguardo() {
		return cteIdResguardo;
	}
    
	public void setCteIdResguardo(CteEntity cteIdResguardo) {
		this.cteIdResguardo = cteIdResguardo;
	}

	public String getIps() {
		return ips;
	}

	public void setIps(String ips) {
		this.ips = ips;
	}

	public CteEntity getCteIdNivSisben() {
		return cteIdNivSisben;
	}

	public void setCteIdNivSisben(CteEntity cteIdNivSisben) {
		this.cteIdNivSisben = cteIdNivSisben;
	}

	public Integer getNumSisben() {
		return numSisben;
	}

	public void setNumSisben(Integer numSisben) {
		this.numSisben = numSisben;
	}

	public CteEntity getCteIdPobVictima() {
		return cteIdPobVictima;
	}

	public void setCteIdPobVictima(CteEntity cteIdPobVictima) {
		this.cteIdPobVictima = cteIdPobVictima;
	}
	
	public String getCerDesplazado() {
		return cerDesplazado;
	}

	public void setCerDesplazado(String cerDesplazado) {
		this.cerDesplazado = cerDesplazado;
	}
	
	public Date getFecDesplazado() {
		return fecDesplazado;
	}

	public void setFecDesplazado(Date fecDesplazado) {
		this.fecDesplazado = fecDesplazado;
	}
	
	public Integer getDvpIdDesplazado() {
		return dvpIdDesplazado;
	}

	public void setDvpIdDesplazado(Integer dvpIdDesplazado) {
		this.dvpIdDesplazado = dvpIdDesplazado;
	}
	
	public String getIdUnico() {
		return idUnico;
	}

	public void setIdUnico(String idUnico) {
		this.idUnico = idUnico;
	}
	

	
	public EstadoAlumnoEntity getEstadoId() {
		return estadoId;
	}

	public void setEstadoId(EstadoAlumnoEntity estadoId) {
		this.estadoId = estadoId;
	}

	public String getNombre1Fnt() {
		return nombre1Fnt;
	}

	public void setNombre1Fnt(String nombre1Fnt) {
		this.nombre1Fnt = nombre1Fnt;
	}

	public String getNombre2Fnt() {
		return nombre2Fnt;
	}

	public void setNombre2Fnt(String nombre2Fnt) {
		this.nombre2Fnt = nombre2Fnt;
	}
	
	public String getApellido1Fnt() {
		return apellido1Fnt;
	}

	public void setApellido1Fnt(String apellido1Fnt) {
		this.apellido1Fnt = apellido1Fnt;
	}

	public String getApellido2Fnt() {
		return apellido2Fnt;
	}

	public void setApellido2Fnt(String apellido2Fnt) {
		this.apellido2Fnt = apellido2Fnt;
	}
	
	public CteEntity getCteIdEspecial() {
		return cteIdEspecial;
	}

	public void setCteIdEspecial(CteEntity cteIdEspecial) {
		this.cteIdEspecial = cteIdEspecial;
	}
	
	public String getVieneSectorPrivado() {
		return vieneSectorPrivado;
	}

	public void setVieneSectorPrivado(String vieneSectorPrivado) {
		this.vieneSectorPrivado = vieneSectorPrivado;
	}

	public String getVieneOtroMunicipio() {
		return vieneOtroMunicipio;
	}

	public void setVieneOtroMunicipio(String vieneOtroMunicipio) {
		this.vieneOtroMunicipio = vieneOtroMunicipio;
	}
	
	public String getInstitucionBienestar() {
		return institucionBienestar;
	}

	public void setInstitucionBienestar(String institucionBienestar) {
		this.institucionBienestar = institucionBienestar;
	}

	public String getSubsidio() {
		return subsidio;
	}

	public void setSubsidio(String subsidio) {
		this.subsidio = subsidio;
	}
	
	public CteEntity getCteIdSituacionAnoAnterior() {
		return cteIdSituacionAnoAnterior;
	}

	public void setCteIdSituacionAnoAnterior(CteEntity cteIdSituacionAnoAnterior) {
		this.cteIdSituacionAnoAnterior = cteIdSituacionAnoAnterior;
	}
	
	public CteEntity getCteIdSangre() {
		return cteIdSangre;
	}

	public void setCteIdSangre(CteEntity cteIdSangre) {
		this.cteIdSangre = cteIdSangre;
	}
	
	public CteEntity getCteIdZona() {
		return cteIdZona;
	}

	public void setCteIdZona(CteEntity cteIdZona) {
		this.cteIdZona = cteIdZona;
	}
	
	public String getNuevoEnInstEd() {
		return nuevoEnInstEd;
	}
	
	public void setNuevoEnInstEd(String nuevoEnInstEd) {
		this.nuevoEnInstEd = nuevoEnInstEd;
	}
	
	public String getAuditUser() {
		return auditUser;
	}

	public void setAuditUser(String auditUser) {
		this.auditUser = auditUser;
	}

	public String getAuditPage() {
		return auditPage;
	}

	public void setAuditPage(String auditPage) {
		this.auditPage = auditPage;
	}

	public Date getAuditDate() {
		return auditDate;
	}

	public void setAuditDate(Date auditDate) {
		this.auditDate = auditDate;
	}

	public CteEntity getCteIdFueRecu() {
		return cteIdFueRecu;
	}

	public void setCteIdFueRecu(CteEntity cteIdFueRecu) {
		this.cteIdFueRecu = cteIdFueRecu;
	}

	public String getCabFamilia() {
		return cabFamilia;
	}

	public void setCabFamilia(String cabFamilia) {
		this.cabFamilia = cabFamilia;
	}

	public String getBenMadFlia() {
		return benMadFlia;
	}

	public void setBenMadFlia(String benMadFlia) {
		this.benMadFlia = benMadFlia;
	}

	public String getBenVetFp() {
		return benVetFp;
	}

	public void setBenVetFp(String benVetFp) {
		this.benVetFp = benVetFp;
	}

	public String getBenHerNac() {
		return benHerNac;
	}

	public void setBenHerNac(String benHerNac) {
		this.benHerNac = benHerNac;
	}

	public Integer getSinebId() {
		return sinebId;
	}

	public void setSinebId(Integer sinebId) {
		this.sinebId = sinebId;
	}

	public Integer getSistema() {
		return sistema;
	}

	public void setSistema(Integer sistema) {
		this.sistema = sistema;
	}

	public Integer getTipCruResgistraduria() {
		return tipCruResgistraduria;
	}

	public void setTipCruResgistraduria(Integer tipCruResgistraduria) {
		this.tipCruResgistraduria = tipCruResgistraduria;
	}

	public CteEntity getCteIdVieneSipi() {
		return cteIdVieneSipi;
	}

	public void setCteIdVieneSipi(CteEntity cteIdVieneSipi) {
		this.cteIdVieneSipi = cteIdVieneSipi;
	}

	public String getIdUnicoRpt() {
		return idUnicoRpt;
	}

	public void setIdUnicoRpt(String idUnicoRpt) {
		this.idUnicoRpt = idUnicoRpt;
	}

	public String getEstSipod() {
		return estSipod;
	}

	public void setEstSipod(String estSipod) {
		this.estSipod = estSipod;
	}

	public String getEstIndigena() {
		return estIndigena;
	}

	public void setEstIndigena(String estIndigena) {
		this.estIndigena = estIndigena;
	}

	public String getSisbenCruce() {
		return sisbenCruce;
	}

	public void setSisbenCruce(String sisbenCruce) {
		this.sisbenCruce = sisbenCruce;
	}

	public String getSifaId() {
		return sifaId;
	}

	public void setSifaId(String sifaId) {
		this.sifaId = sifaId;
	}

	public String getProcesado() {
		return procesado;
	}

	public void setProcesado(String procesado) {
		this.procesado = procesado;
	}

	public String getPorSisben() {
		return porSisben;
	}

	public void setPorSisben(String porSisben) {
		this.porSisben = porSisben;
	}

	public CteEntity getCteIdPaisOrigen() {
		return cteIdPaisOrigen;
	}

	public void setCteIdPaisOrigen(CteEntity cteIdPaisOrigen) {
		this.cteIdPaisOrigen = cteIdPaisOrigen;
	}

	public Date getFecVenDoc() {
		return fecVenDoc;
	}

	public void setFecVenDoc(Date fecVenDoc) {
		this.fecVenDoc = fecVenDoc;
	}

	public Integer getGemelo() {
		return gemelo;
	}

	public void setGemelo(Integer gemelo) {
		this.gemelo = gemelo;
	}

	public Integer getIdBenSsdipi() {
		return idBenSsdipi;
	}

	public void setIdBenSsdipi(Integer idBenSsdipi) {
		this.idBenSsdipi = idBenSsdipi;
	}

	public CteEntity getCteIdSisbenIv() {
		return cteIdSisbenIv;
	}

	public void setCteIdSisbenIv(CteEntity cteIdSisbenIv) {
		this.cteIdSisbenIv = cteIdSisbenIv;
	}

	public CteEntity getCteIdDocVenezolano() {
		return cteIdDocVenezolano;
	}

	public void setCteIdDocVenezolano(CteEntity cteIdDocVenezolano) {
		this.cteIdDocVenezolano = cteIdDocVenezolano;
	}

	public Integer getNumVenezolano() {
		return numVenezolano;
	}

	public void setNumVenezolano(Integer numVenezolano) {
		this.numVenezolano = numVenezolano;
	}
	
}
