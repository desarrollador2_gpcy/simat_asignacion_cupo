package com.simat_asignacion_cupo.client;

import com.simat_asignacion_cupo.client.dto.PersonasEstadoEntity;

public interface ClientRestEstadoAlumno {

	public PersonasEstadoEntity obtenerEstadoAlumno(String idPersona, String url);
}
