package com.simat_asignacion_cupo.client;

import java.util.List;

import com.simat_asignacion_cupo.client.dto.InstitucionEntity;

public interface ClientRestValidarDaneEstablecimiento {

	List<InstitucionEntity> validarDaneEstablecimiento(String codMunicipio);
}
