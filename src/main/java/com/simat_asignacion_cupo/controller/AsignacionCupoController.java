package com.simat_asignacion_cupo.controller;

import java.util.List;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.simat_asignacion_cupo.client.ClientRestEstadoAlumno;
import com.simat_asignacion_cupo.client.ClientRestEtapa;
import com.simat_asignacion_cupo.client.ClientRestListadoSedes;
import com.simat_asignacion_cupo.client.ClientRestValidarCupo;
import com.simat_asignacion_cupo.client.ClientRestValidarDaneEstablecimiento;
import com.simat_asignacion_cupo.client.ClientRestValidarEstadoInscripcion;
import com.simat_asignacion_cupo.client.ClientRestValidarPersona;
import com.simat_asignacion_cupo.client.dto.EtapaEntity;
import com.simat_asignacion_cupo.client.dto.Message;
import com.simat_asignacion_cupo.client.dto.PersonasEntity;
import com.simat_asignacion_cupo.client.dto.PersonasEstadoEntity;
import com.simat_asignacion_cupo.client.dto.SedeEntity;
import com.simat_asignacion_cupo.entity.AsignacionCupoIn;
import com.simat_asignacion_cupo.entity.GruposEntityAsig;
import com.simat_asignacion_cupo.service.AsignacionCupoService;
import com.simat_asignacion_cupo.utils.PropertiesAsignacion;


@RestController
@RequestMapping("/cupo")
public class AsignacionCupoController {
	
	
	@Autowired
	AsignacionCupoService asignacionCupoService;
	
	@Autowired
	ClientRestEstadoAlumno  clientRestEstadoAlumno;

	@Autowired
	ClientRestEtapa clientRestEtapa;
	
	@Autowired
	ClientRestValidarPersona clientRestValidarPersona;
	
	@Autowired
	ClientRestValidarDaneEstablecimiento clientRestValidarDaneEstablecimiento;
	
	@Autowired
	ClientRestValidarEstadoInscripcion clientRestValidarEstadoInscripcion;
	
	@Autowired
	ClientRestValidarCupo clientRestValidarCupo;
		
	@Autowired
	ClientRestListadoSedes clientRestListadoSedes;
	
	public static final String COD_JERARQUIA="1";
	
	Properties propiedades = new Properties();
	
	@RequestMapping(value = "/postAsignacioncupo", method = RequestMethod.POST)
	public ResponseEntity postAsignacionCupo(
			@RequestBody AsignacionCupoIn asignacionCupoIn )  {		
		
		try {


			PropertiesAsignacion prop= new PropertiesAsignacion();
	            String codJerarquia= prop.getProperty("asignacion.code.jerarquia");
	            Message message= new Message();
			/**
			 * 1. Servicio para valida etapa activa 
			 *Nota: No veo de donde sacar etapa y jerarquia. Calendario viene en la entrada(confirmar)
			 * return anio etapa, valido que sea el anio actual
			 * Rta: Pendiente cambiar por servicio nuevo
			 * Servicio dado el DANE, devuelve datos de la institución para obtener el calendario(codDane)
			 * Cambiar por servicio que devuleva la etapa dado un DANE 
			 */
			List<EtapaEntity> institucionesEntities =clientRestEtapa.getProcesoByDaneAnoEtapa(asignacionCupoIn.getCodDane(),asignacionCupoIn.getAnio(), prop.getProperty("asignacion.code.etapa"), prop.getProperty("asignacion.url.procesobydaneanoetapa"));
			if(institucionesEntities!=null&&institucionesEntities.size()>0&&institucionesEntities.get(0).getAbierta().equals("S")) {
				
			
				/**
				 * 2. Servicio para validar que la persona exista
				 * 
				 */
	            List<PersonasEntity> persona= clientRestValidarPersona.validarPersonaExiste(asignacionCupoIn.getTipoId(), asignacionCupoIn.getNumeroId(), asignacionCupoIn.getNombre1(), asignacionCupoIn.getNombre2(),asignacionCupoIn.getApellido1() , asignacionCupoIn.getApellido2(),asignacionCupoIn.getFechaNacimiento(),prop.getProperty("asignacion.url.validarpersonaexiste"));
				if(persona!=null&&persona.size()>0 &&persona.get(0).getId()!=null) {
				
					/***
					 * 3. Servicio que devuelve el id del establecimiento y el id de la sede  
					 * Nota: No veo el codMunicipio en la entrada, en donde se usan datos de salida?
					 * Servicio se le envía daneSede y daneInstitucion, tabla INSTITUCIONES para validar si es valido
					 */
					clientRestEtapa.getInstitucionPublicaByDane(asignacionCupoIn.getCodDane(),prop.getProperty("asignacion.url.institucionpublicabydane"));
						
					
					//http://52.43.129.135:8085/simat/swagger-ui/index.html?configUrl=/simat/v3/api-docs/swagger-config#/sede-controller/getListaSedes
					
					
					List<SedeEntity> sedeEntities= clientRestListadoSedes.getListadoSedes(asignacionCupoIn.getCodDane(),asignacionCupoIn.getCodDaneSede(),prop.getProperty("asignacion.url.validarsedes"));
					if(sedeEntities!=null&&sedeEntities.size()>0) {
					//Se reemplaza por getInstitucionPublicaByDane
					//clientRestValidarDaneEstablecimiento.validarDaneEstablecimiento(apellido2); validar?
					
					
					/***
					 * 4. Servicio para validar que la inscripción sea nueva
					 * Nota: Debo poner el anio y el dane_establecimiento, inscripción, join opciones
					 * 1. Debo validar que la inscripción este en estado NUEVA 985
					 * 2. Con el campo Dane validar institucion INS_ID JOIN OPCIONES con Instituciones 
					 * y se busca por codDane,  
					 * validar que la inscripción este abierta y que uno de las instituciones corresponda a ese DANE
					 * Validar que la inscripción este abierta y que la opción si sea, codDane,*/
					 
					 int result= clientRestValidarEstadoInscripcion.validarEstadoInscripcion(persona.get(0).getId(), asignacionCupoIn.getCodDane(), asignacionCupoIn.getAnio(),prop.getProperty("asignacion.url.validarestadoinscripcion"));
					 if(result>0) {
					
						/***
						 * 5.Servicio para validar que último estado del alumno sea del año de la inscripción y
						 *  que el estado sea inscrito en ese año  
						 * Debo validar que la inscripción este en estado INSCRITO 959 y que el estado sea del anio
						 */
						 
							 PersonasEstadoEntity personaEstado= clientRestEstadoAlumno.obtenerEstadoAlumno(persona.get(0).getId().toString(), prop.getProperty("asignacion.url.obtenerestadoalumno"));
							 if(personaEstado.getCteIdEstado()==959&&personaEstado.getAno()==asignacionCupoIn.getAnio()) {
							 /***
							  * 6. Servicio para validar si hay cupo
							  * Si capacidad - asignados>0, hay cupo
							  * 
							  */
								 GruposEntityAsig gruposEntity= clientRestValidarCupo.validarCupo(asignacionCupoIn.getAnio(), asignacionCupoIn.getCodGrado(), asignacionCupoIn.getCodMetodologia(), asignacionCupoIn.getCodJornada(), asignacionCupoIn.getCodDane(),prop.getProperty("asignacion.url.validarcupo"));
								 if(gruposEntity!=null&&gruposEntity.getCapacidad()-gruposEntity.getAsignados()>0) {
									 try {
										 System.out.print("perId " +persona.get(0).getId()+" grupo "+ gruposEntity.getIdGrupo());
										asignacionCupoService.postAsignacionCupo(persona.get(0).getId(), asignacionCupoIn.getAnio(), gruposEntity.getIdGrupo(), asignacionCupoIn.getCodEspecialidad(), asignacionCupoIn.getFuenteRecurso(), asignacionCupoIn.getMatriculaContratada(), prop.getProperty("asignacion.usuario"), prop.getProperty("asignacion.pagina"), prop.getProperty("asignacion.comentario"));
									
									 }catch (Exception e) {
										System.out.println("error "+ e.getMessage());
										e.getCause();
								        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
									}
									 
								 }else {
									
									 message.setMensaje("No hay cupos disponibles");
									 return ResponseEntity.status(HttpStatus.BAD_REQUEST)
												.body(message);
								 }
							 }else {
								 message.setMensaje("El alumno no se encuentra en el estado correcto");
								 return ResponseEntity.status(HttpStatus.BAD_REQUEST)
											.body(message);
							 }
							 
						 }else {
							 message.setMensaje("La inscripción no se encuentra en el estado correcto / Dane invalido");
							 return ResponseEntity.status(HttpStatus.BAD_REQUEST)
										.body(message);
						 }
					}else {
						 message.setMensaje("La sede/institución no coinciden");
						 return ResponseEntity.status(HttpStatus.BAD_REQUEST)
									.body(message);
					 }
					}else {
						message.setMensaje("La persona no existe");
						 return ResponseEntity.status(HttpStatus.BAD_REQUEST)
								.body(message);
					}
			}else {
				message.setMensaje("La etapa no se encuentra abierta");
				 return ResponseEntity.status(HttpStatus.BAD_REQUEST)
						.body(message);
			}
			 message.setMensaje("Cupo asignado con exito");
	        return ResponseEntity.status(HttpStatus.OK).body(message);

		} catch (Exception e) {
			System.out.println("Error "+e.getMessage() + e.getClass());
			return ResponseEntity
	                .status(HttpStatus.INTERNAL_SERVER_ERROR)
	                .body("Error Message "+e.getMessage() + e.getClass());
		}
	}
}
