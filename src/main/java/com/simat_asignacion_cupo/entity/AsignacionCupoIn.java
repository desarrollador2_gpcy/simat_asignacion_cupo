package com.simat_asignacion_cupo.entity;

public class AsignacionCupoIn {
	
	private String calendario;
	private int anio;
	private String codSecretariaEducacion;
	private String codMetodologia;
	private String codGrado;
	private String codJornada;
	private String codDane;
	private String codDaneSede;
	private int tipoId;
	private String numeroId; 
	private String nombre1;
	private String nombre2; 
	private String apellido1;
	private String apellido2;
	private String fechaNacimiento;
	private String codEspecialidad;
	private String fuenteRecurso;
	private String matriculaContratada;
	
	
	public String getCalendario() {
		return calendario;
	}
	public void setCalendario(String calendario) {
		this.calendario = calendario;
	}
	public int getAnio() {
		return anio;
	}
	public void setAnio(int anio) {
		this.anio = anio;
	}
	public String getCodSecretariaEducacion() {
		return codSecretariaEducacion;
	}
	public void setCodSecretariaEducacion(String codSecretariaEducacion) {
		this.codSecretariaEducacion = codSecretariaEducacion;
	}
	public String getCodMetodologia() {
		return codMetodologia;
	}
	public void setCodMetodologia(String codMetodologia) {
		this.codMetodologia = codMetodologia;
	}
	public String getCodGrado() {
		return codGrado;
	}
	public void setCodGrado(String codGrado) {
		this.codGrado = codGrado;
	}
	public String getCodJornada() {
		return codJornada;
	}
	public void setCodJornada(String codJornada) {
		this.codJornada = codJornada;
	}
	public String getCodDane() {
		return codDane;
	}
	public void setCodDane(String codDane) {
		this.codDane = codDane;
	}
	public int getTipoId() {
		return tipoId;
	}
	public void setTipoId(int tipoId) {
		this.tipoId = tipoId;
	}
	public String getNumeroId() {
		return numeroId;
	}
	public void setNumeroId(String numeroId) {
		this.numeroId = numeroId;
	}
	public String getNombre1() {
		return nombre1;
	}
	public void setNombre1(String nombre1) {
		this.nombre1 = nombre1;
	}
	public String getNombre2() {
		return nombre2;
	}
	public void setNombre2(String nombre2) {
		this.nombre2 = nombre2;
	}
	public String getApellido1() {
		return apellido1;
	}
	public void setApellido1(String apellido1) {
		this.apellido1 = apellido1;
	}
	public String getApellido2() {
		return apellido2;
	}
	public void setApellido2(String apellido2) {
		this.apellido2 = apellido2;
	}
	public String getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public String getCodEspecialidad() {
		return codEspecialidad;
	}
	public void setCodEspecialidad(String codEspecialidad) {
		this.codEspecialidad = codEspecialidad;
	}
	public String getFuenteRecurso() {
		return fuenteRecurso;
	}
	public void setFuenteRecurso(String fuenteRecurso) {
		this.fuenteRecurso = fuenteRecurso;
	}
	public String getMatriculaContratada() {
		return matriculaContratada;
	}
	public void setMatriculaContratada(String matriculaContratada) {
		this.matriculaContratada = matriculaContratada;
	}
	public String getCodDaneSede() {
		return codDaneSede;
	}
	public void setCodDaneSede(String codDaneSede) {
		this.codDaneSede = codDaneSede;
	}
	
	
	


}
