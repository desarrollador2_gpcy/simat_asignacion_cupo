package com.simat_asignacion_cupo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.simat_asignacion_cupo.entity.AsignacionCupoIn;
import com.simat_asignacion_cupo.repository.AsignacionCupoRepository;
import com.simat_asignacion_cupo.service.AsignacionCupoService;

@Service
public class AsignacionCupoServiceImpl implements AsignacionCupoService{

	
	@Autowired
	private AsignacionCupoRepository asignacionCupoRepository;

	public void postAsignacionCupo(int perId, int ano, int idGroup, String codigo,String codigo2, String subsidio, String auditUser, String page, String comentario) {
		 asignacionCupoRepository.postAsignacionCupo(perId, ano, idGroup, codigo,codigo2, subsidio, auditUser, page, comentario);
	}
}
