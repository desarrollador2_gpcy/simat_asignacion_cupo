package com.simat_asignacion_cupo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.simat_asignacion_cupo.entity.GruposEntityAsig;


@Repository
public interface AsignacionCupoRepository extends JpaRepository<GruposEntityAsig, String> {
	
	  @Procedure(value = "PKG_ESTADO_ALUMNO.PR_CAMBIAR_AS")
	  void postAsignacionCupo(@Param("p_per_id")int perId, @Param("p_anno")int ano,@Param("p_grp_id") int idGroup, @Param("p_cod_especialidad")String codigo,@Param("p_cod_fue_recu")String codigo2, @Param("p_matricula_contratada")String subsidio, @Param("p_audit_user")String auditUser, @Param("p_audit_page")String page, @Param("p_comentario")String comentario);


  }